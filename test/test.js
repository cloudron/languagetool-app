#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    timers = require('timers/promises');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let app, prefix = 'v2';

    before(function () {
        if (!process.env.USERNAME) throw new Error('USERNAME env var not set');
        if (!process.env.PASSWORD) throw new Error('PASSWORD env var not set');
    });

    after(function () {
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function checkLanguages() {
        const result = await superagent.get(`https://${app.fqdn}/${prefix}/languages`).ok(() => true);
        if (result.statusCode !== 200) throw new Error('Languages list failed with status ' + result.statusCode);
        if (result.body.length < 1) throw new Error('No languages returned:' + result.statusCode);
        console.log(`Languages returned: ${result.body.length}`);
    }

    async function changeUrlPrefix() {
        fs.writeFileSync('/tmp/languagetool-test-env', 'API_PATH_PREFIX=SchonhauserArcade\n', { encoding: 'utf8' });
        execSync(`cloudron push --app ${app.id} /tmp/languagetool-test-env /app/data/env`);
        execSync(`cloudron restart --app ${app.id}`);
        prefix = 'SchonhauserArcade/v2';
    }

    async function installNgrams() {
        console.log('Installing ngrams, this takes a while. see /tmp/ngrams.txt');
        return new Promise((resolve, reject) => {
            const stream = new fs.createWriteStream('/tmp/ngrams.txt');
            stream.on('open', function() {
                execSync(`cloudron exec --app ${app.id} -- /app/pkg/install-ngrams.sh /app/data/ngrams en`, { encoding: 'utf8', stdio:['ignore', stream, stream] });
                resolve();
            });
            stream.on('error', reject);
        });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        console.log('waiting for 30s for app to come up');
        await timers.setTimeout(30000); // nginx comes up before the app
    });

    it('can get app information', getAppInfo);
    it('check Languages', checkLanguages);
    it('can change url', changeUrlPrefix);
    it('check languages', checkLanguages);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(15000); // nginx comes up before the app
    });
    it('check Languages', checkLanguages);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(15000); // nginx comes up before the app
    });

    it('check Languages', checkLanguages);
    it('can install ngrams', installNgrams);

    it('move to different location', async function () {
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(15000);
    });
    it('can get app information', getAppInfo);
    it('check Languages', checkLanguages);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app', async function () {
        execSync(`cloudron install --appstore-id org.languagetool.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        await timers.setTimeout(15000); // nginx comes up before the app
    });
    it('can get app information', getAppInfo);
    it('can change url', changeUrlPrefix);
    it('check Languages', checkLanguages);

    it('can update', async function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(15000); // nginx comes up before the app
    });
    it('check Languages', checkLanguages);
    it('uninstall app', async function () {
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
