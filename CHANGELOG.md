[0.1.0]
* Initial version for LanguageTool 6.0

[0.2.0]
* Add more app meta info

[0.3.0]
* Package script cleanups
* Automatically restart language tool after n-gram downloads

[1.0.0]
* Initial stable version

[1.1.0]
* Bump java heap size

[1.2.0]
* Update LanguageTool to 6.1
* [Full changelog](https://github.com/languagetool-org/languagetool/releases/tag/v6.1)

[1.3.0]
* Update LanguageTool to 6.2
* [Full changelog](https://github.com/languagetool-org/languagetool/releases/tag/v6.2)

[1.4.0]
* Update base image to 4.2.0

[1.4.1]
* Update LanguageTool to 6.4
* [Full changelog](https://github.com/languagetool-org/languagetool/releases/tag/v6.4)

[1.5.0]
* add fasttext support for language detection

[1.5.1]
* Update LanguageTool to 6.5
* [Full changelog](https://github.com/languagetool-org/languagetool/releases/tag/v6.5)

