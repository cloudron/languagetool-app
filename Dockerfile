FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

# https://adoptium.net/blog/2021/12/eclipse-temurin-linux-installers-available/
RUN wget -qO - https://packages.adoptium.net/artifactory/api/gpg/key/public | apt-key add -
RUN apt-get update -y && \
    apt-get install -y software-properties-common xmlstarlet libtcnative-1 fasttext && \
    add-apt-repository --yes 'https://packages.adoptium.net/artifactory/deb jammy main' && \
    apt-get update -y && \
    apt-get install -y temurin-11-jdk && \
    rm -r /var/cache/apt /var/lib/apt/lists

# renovate: datasource=github-tags depName=languagetool-org/languagetool versioning=regex:^(?<major>\d+)\.(?<minor>\d+)$ extractVersion=^v(?<version>.+)$
ENV LT_VERSION=6.5

RUN mkdir -p /app/code /app/pkg/nginx
WORKDIR /app/code

RUN wget https://www.languagetool.org/download/LanguageTool-$LT_VERSION.zip && \
    unzip LanguageTool-$LT_VERSION.zip -d /tmp && \
	mv /tmp/LanguageTool-$LT_VERSION/* /app/code/ && \
	chown -R cloudron:cloudron /app/code && \
    rm -rf LanguageTool-$LT_VERSION.zip /tmp/LanguageTool-$LT_VERSION

# fasttext model (https://fasttext.cc/docs/en/language-identification.html)
RUN wget https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.bin -O /app/pkg/lid.176.bin

# add nginx config
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
RUN ln -sf /run/nginx/languagetool-nginx.conf /etc/nginx/sites-enabled/languagetool.conf
COPY nginx/readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
COPY nginx/languagetool.conf /app/pkg/languagetool-nginx.conf

# add placeholder page assets
COPY www /app/pkg/www
COPY logo.png /app/pkg/www/logo.png

# supervisor
COPY supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY config.properties start.sh install-ngrams.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
