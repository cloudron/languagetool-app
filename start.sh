#!/bin/bash

set -eu

mkdir -p /run/nginx

if [[ ! -f /app/data/env ]]; then
    echo "# Protect installation with magic/hidden URL" > /app/data/env
    echo "API_PATH_PREFIX=" >> /app/data/env
    echo "# Activate n-gram datasets (https://docs.cloudron.io/apps/languagetool/#n-grams)" >> /app/data/env
    echo "NGRAMS_DATASET_PATH=/app/data/ngrams" >> /app/data/env
fi

source /app/data/env

export NGRAMS_DATASET_PATH="${NGRAMS_DATASET_PATH:-/app/data/ngrams}"
mkdir -p "${NGRAMS_DATASET_PATH}"

cp /app/pkg/languagetool-nginx.conf /run/nginx/languagetool-nginx.conf
if [[ -n "${API_PATH_PREFIX:-}" ]]; then
    sed -e "s,\(v2\),${API_PATH_PREFIX}," \
        -e "s,# rewrite /custom,rewrite /${API_PATH_PREFIX}," \
        -i /run/nginx/languagetool-nginx.conf
fi

[[ ! -f /app/data/config.properties ]] && cp /app/pkg/config.properties /app/data/config.properties

echo "=> Fixing permissions"
chown cloudron:cloudron -R /app/data

echo "=> Starting LanguageTool using supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i LanguageTool
