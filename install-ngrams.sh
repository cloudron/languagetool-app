#!/bin/bash

set -eu

if [[ $# -lt 2 ]]; then
    echo "Usage: install-ngrams.sh <path> <languages...>" >&2
    echo "Example: install-ngrams.sh /app/data/ngrams en es gb" >&2
    echo "Supported languages: en de es fr nl"
    exit 2
fi

NGRAMS_DATASET_PATH=$1
mkdir -p "${NGRAMS_DATASET_PATH}"

url="https://languagetool.org/download/ngram-data/ngrams-en-20150817.zip"
for language in "${@:2}";
do
    case "${language}" in
        "en" ) url="https://languagetool.org/download/ngram-data/ngrams-en-20150817.zip";;
        "de" ) url="https://languagetool.org/download/ngram-data/ngrams-de-20150819.zip";;
        "es" ) url="https://languagetool.org/download/ngram-data/ngrams-es-20150915.zip";;
        "fr" ) url="https://languagetool.org/download/ngram-data/ngrams-fr-20150913.zip";;
        "nl" ) url="https://languagetool.org/download/ngram-data/ngrams-nl-20181229.zip";;
         *   ) echo "No such language ${language}"; exit 3;;
    esac
    echo "==> Installing ${language} ngram dataset from ${url}"
    wget "${url}" --quiet --show-progress -O "/tmp/${language}.zip"

    echo "==> Unpacking ${language} ngram dataset"
    cd "${NGRAMS_DATASET_PATH}"
    unzip "/tmp/${language}.zip"
    rm -f "/tmp/${language}.zip"
    echo "==> ${language} ngram dataset has been installed."
done

chown cloudron:cloudron -R "${NGRAMS_DATASET_PATH}"

echo -e "==> Done\n\n"
echo "Be sure to set NGRAMS_DATASET_PATH=${NGRAMS_DATASET_PATH} in /app/data/env and restart the app."

