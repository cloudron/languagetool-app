This app is pre-setup with `/v2` as the API URL.

Please [change](https://docs.cloudron.io/apps/languagetool/#security) the URL immediately to prevent misuse.

